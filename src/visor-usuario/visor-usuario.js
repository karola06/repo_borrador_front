import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../visor-cuenta-lista/visor-cuenta-lista.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*el estilo no entra al componente ----
              all: initial; */
          border: solid blue;
        }

        .redbg {
          background-color: red;
        }
        .bluebg {
          background-color: blue;
        }
        .greenbg {
          background-color: green;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--      <div class="row greybg">
      </div>
      <button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>
      <button class="btn btn-danger">Logout</button>
      <button class="btn btn-warning">Logout</button>
      <button class="btn btn-primary btn-lg">Logout</button>
      <button class="btn btn-secondary btn-sm">Logout</button>
      <button class="btn btn-light">Logout</button>
      <button class="btn btn-dark">Logout</button>   -->
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>y mi email es [[email]]</h2>
      <button class="btn btn-info" on-click="showcuentas">Cuentas</button>
<!--      <visor-cuenta-lista id="19"></visor-cuenta-lista>  -->
      <visor-cuenta-lista id=visorcuentas></visor-cuenta-lista>

<!--      <iron-ajax
        auto
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{idcuenta}}"
        handle-as="json"
        on-response="showData"
      >   -->
      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{idusuario}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  _idusuarioChanged(newValue, oldValue){
    // comunica con el back-end lanzando la petición
    this.$.getUser.generateRequest();
    console.log("idUsuario value has changed");
    console.log("Old value was " + oldValue);
    console.log("New value is " + newValue);
  }

  showcuentas(){
    // comunica con el back-end lanzando la petición
    this.$.visorcuentas.idusuario = this.idusuario;
    this.$.getUser.generateRequest();

  }

  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, idusuario: {
        type: Number,
        observer: "_idusuarioChanged"
      }
    };
  }  // End properties

// función showData manejadora de la respuesta
  showData(data){
    console.log("*********showData");
    console.log(data.detail.response.first_name);
    this.first_name=data.detail.response.first_name;
    this.last_name=data.detail.response.last_name;
    this.email=data.detail.response.email;
  }

  showError(error){
    console.log("Hubo un error ");
    console.log(error);
  }  // End showError

}  //  End Class

// define una etiqueta y la asocia con la clase (FrontproyectoApp)
window.customElements.define('visor-usuario', VisorUsuario);
