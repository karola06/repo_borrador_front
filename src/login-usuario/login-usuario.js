import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <input type="email" placeholder="email" value="{{email::input}}"/>
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="login">Login</button>
      <span hidden$="[[!isLogged]]">Bienvenid@ de nuevo</span>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error ="showError"
      >
      </iron-ajax>
    `;
  }

//  en iro-ajax si el startus es 200 o 300 lanza el on-response, si no lanza el showError

  static get properties() {
    return {
      password: {
        type: String
      }, email: {
        type: String
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }  //  End properties

  login(){
    console.log("El usuario ha pulsado el botón");
    var loginData = {
      "email" : this.email,
      "password" : this.password
    }
    //  this.$ es para acceder a los elementos del html
    this.$.doLogin.body = JSON.stringify(loginData);
    // comunica con el back-end lanzando la petición
    this.$.doLogin.generateRequest();
    console.log("login data es ");
    console.log(loginData);
  }   //End login

  manageAJAXresponse(data){
    console.log("Llegaron los resultados ");
    console.log(data.detail.response);
    this.isLogged = true;
  //  si el login es correcto lanzo el evento
    console.log("Botón pulsado");
//    console.log(e);
    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "idusuario" : data.detail.response.idUsuario
          }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
        }
      )  //end parámetros new CustomEvent
    )  //end parámetros dispatchEvent

  }   //End manageAJAXresponse(

  showError(error){
    console.log("Hubo un error ");
    console.log(error);
  }  // End showError

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('login-usuario', LoginUsuario);
