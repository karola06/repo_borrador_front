import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*el estilo no entra al componente ----
              all: initial; */
          border: solid blue;
        }
        .redbg {
          background-color: red;
        }
        .bluebg {
          background-color: blue;
        }
        .greenbg {
          background-color: green;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <div class="row greybg">
      <h1 style= "color : white"> Consulta </h1>
      </div>
      <div>
      <div class="offset-1"><h3>Cuenta :   [[iban]]</h3></div>
      <div class="offset-1"><h3>Balance:   [[balance]]</h3></div>
      </div>

      <iron-ajax
        auto
        id="getCuentas"
        url="http://localhost:3000/apitechu/v2/cuentas/{{id}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      id: {
        type: Number
      }, iban: {
        type: String
      }, balance: {
        type: Number
      }
    };
  }  // End properties

// función showData manejadora de la respuesta
  showData(data){
    console.log("showData");
    console.log(data.detail.response[0].IBAN);
    console.log(data.detail.response[0].balance);
    this.iban=data.detail.response[0].IBAN;
    this.balance=data.detail.response[0].balance;
  }

}  //  End Class

// define una etiqueta y la asocia con la clase (FrontproyectoApp)
window.customElements.define('visor-cuenta', VisorCuenta);
