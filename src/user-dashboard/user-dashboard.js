import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../emisor-evento/emisor-evento.js'
import '../receptor-evento/receptor-evento.js'
import '../login-usuario/login-usuario.js'
import '../visor-usuario/visor-usuario.js'


/**
 * @customElement
 * @polymer
 */
class UserDashBoard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1>Yo soy tu padre</h1>
      <!--  myevent  está en CustomEvent de emisor-evento que es el que lo lanza -->
      <login-usuario on-myevent="processEvent"></login-usuario>
      <visor-usuario id="receiver"></visor-usuario>
<!--      <emisor-evento on-myevent="processEvent"></emisor-evento>
      <receptor-evento  id="receiver"></receptor-evento>  -->
    `;
  }
  static get properties() {
    return {
    };
  }  //  End properties

  processEvent(e){
    console.log("Capturado evento del emisor");
    console.log(e);
    this.$.receiver.idusuario = e.detail.idusuario;
    console.log ("Id Usuario " + e.detail.idusuario);
  }

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('user-dashboard', UserDashBoard);
