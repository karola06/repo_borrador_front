import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentaLista extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*el estilo no entra al componente ----
              all: initial; */
          border: solid blue;
        }

        .redbg {
          background-color: red;
        }
        .bluebg {
          background-color: blue;
        }
        .greenbg {
          background-color: green;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!--      <div class="col-2 offset-1 col-sm-6 redbg">Col 1</div>
        <div class="col-3 offset-5 col-sm-1 greenbg">Col 2</div>
        <div class="col-4 offset-3 col-sm-1 bluebg">Col 3</div> -->
      <div> <h3>Lista de cuentas:</h3> </div>
<!--      <dom-repeat items="{{cuentas}}">
        <template>
          <div>Cuentas: <span>{{item.IBAN}}</span></div>
          <div>Saldo: <span>{{item.balance}}</span></div>
        </template>
      </dom-repeat>   -->

<!--      <dom-repeat items="{{cuentas}}">
        <template>
          <ul class="list-group list-group-horizontal">
            <li class="list-group-item">{{item.IBAN}}</li>
            <li class="list-group-item">>{{item.balance}}</li>
          </ul>
        </template>
      </dom-repeat>   -->

<div class="table-responsive-sm">
      <table class="table table-sm table-striped">
        <thead>
          <tr>
            <th scope="col">Cuenta</th>
            <th scope="col">Saldo</th>
          </tr>
        </thead>
        <tbody>
          <template is="dom-repeat" items="{{cuentas}}">
            <tr>
              <td>{{item.IBAN}}</td>
              <td>{{item.balance}}</td>
            </tr>
          </template>
        </tbody>
      </table>
  </div>

      <iron-ajax
        id="getCuentas"
        url="http://localhost:3000/apitechu/v2/cuentas/{{idusuario}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      idusuario: {
        type: Number,
        observer: "_idusuarioChanged"
      }, cuentas: {
      type : Array
      }
    };
  }  // End properties

  _idusuarioChanged() {
     this.$.getCuentas.generateRequest();
   }

// función showData manejadora de la respuesta
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
//    console.log(data.detail.response.IBAN);
//    console.log(data.detail.response.balance);
    this.cuentas=data.detail.response;
  }

}  //  End Class

// define una etiqueta y la asocia con la clase (FrontproyectoApp)
window.customElements.define('visor-cuenta-lista', VisorCuentaLista);
