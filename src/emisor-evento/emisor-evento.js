import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h3>Soy el emisor</h3>
      <button on-click="sendEvent">No pulsar</button>
    `;
  }
  static get properties() {
    return {
    };
  }  //  End properties

  sendEvent(e){
    console.log("Botón pulsado");
    console.log(e);
    this.dispatchEvent(
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "course" : "TechU",
            "year" : 2019
          }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
        }
      )  //end parámetros new CustomEvent
    )  //end parámetros dispatchEvent
  }  // end sendEvent

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('emisor-evento', EmisorEvento);
